﻿using System;
using Prog2;

namespace Devinette
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var germe = 0;
            if (args.Length != 0)
            {
                germe = Convert.ToInt32(args[0]);
            }
            
            var r1 = new Random(germe);

            var dateATrouver = Date.Aléatoire(r1, 1700, DateTime.Now.Year);


            var reponse = true;

            while (reponse)
            {
                ConsolePlus.ColorWriteLine(ConsoleColor.Yellow, "J'ai choisi une date aléatoirement entre le 1er janvier 1700 et aujourd'hui..." +
                                                                "\n\nPouvez-vous trouver laquelle?");

                for (int i = 1;; i++)
                {
                    
                    DateUtil.LireDate("Date:", int.MinValue.ToString(), int.MaxValue.ToString(), out Date dateDevinée);
                    
                    Date.TryParse(dateDevinée.ToString(), out Date dateJoueur);

                    if (dateJoueur.ComparerAvec(dateATrouver) == 0)
                    {
                        ConsolePlus.ColorWriteLine(ConsoleColor.Green, $"Bravo! Vous avez trouvé la date en {i} essais!");
                        break;
                    }

                    if (dateJoueur.ComparerAvec(dateATrouver) == 1)
                    {
                        ConsolePlus.ColorWriteLine(ConsoleColor.Yellow, "Trop grand!");
                    }

                    if (dateJoueur.ComparerAvec(dateATrouver) == -1)
                    {
                        ConsolePlus.ColorWriteLine(ConsoleColor.Yellow, "Trop petit!");
                    }
                }

                ConsolePlus.LireBooleen("Rejouer?", out reponse);
            }


            ConsolePlus.ColorWriteLine(ConsoleColor.DarkMagenta, "Merci d'avoir joué");
        }
    }
}