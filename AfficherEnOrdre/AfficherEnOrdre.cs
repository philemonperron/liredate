﻿using System;
using Prog2;

namespace AfficherEnOrdre
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Dix dates aléatoires:");
            var random = new Random(0);

            Date[] dates = new Date[10];

            Calendrier[] calendriers = new Calendrier[10];

            for (int i = 0; i < 10; i++)
            {
                int année = random.Next(1700, 2019);
                int mois = random.Next(1, 12);
                int jour = random.Next(1, DateUtil.NbJoursDsMois(année, mois));

                dates[i] = new Date(année, mois, jour);

                ConsolePlus.ColorWriteLine(ConsoleColor.DarkYellow, $" * {DateUtil.EnTexte(dates[i])}");
            }

            for (int i = 0; i < 10; i++)
            {
                int année = random.Next(1700, 2019);
                int mois = random.Next(1, 12);


                calendriers[i] = new Calendrier(année, (Mois) mois);
            }
            
            Array.Sort(dates, (a,b) => b.ComparerAvec(a));
            Console.WriteLine("Dix dates aleatoires en ordre");
            foreach (var date in dates)
            {
                ConsolePlus.ColorWriteLine(ConsoleColor.DarkCyan, " * {0}", date);
            }
            
            Console.WriteLine();
            Array.Sort(dates);
            Console.WriteLine("Dix dates aleatoires en ordre sans lambda");
            foreach (var date in dates)
            {
                ConsolePlus.ColorWriteLine(ConsoleColor.Green, " * {0}", date);
            }
        }
    }
}