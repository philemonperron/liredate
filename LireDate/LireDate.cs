﻿using System;
using System.Diagnostics;
using Prog2;
using static Prog2.DateUtil;

namespace LireDate
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "LireDate";

            Date date;
            if (args.Length == 0)
            {
                var dateValide = DateUtil.LireDate("", "1000-01-01", "3000-01-01", out date);
                if (!dateValide)
                {
                    Environment.Exit(1);
                }
            }
            else
            {
                var strDate = string.Join(" ", args);
                var dateValide = Date.TryParse(strDate, out date);

                if (!dateValide)
                {
                    ConsolePlus.ColorWriteLine(ConsoleColor.DarkRed, "Date invalide {0}", strDate);
                    return;
                }
            }
            
            Console.WriteLine();
            ConsolePlus.Afficher("    Format G", $"{date:G}");
            ConsolePlus.Afficher("    Format -", $"{date:-}");
            ConsolePlus.Afficher("    Format L", $"{date:L}");
            ConsolePlus.Afficher("    Format S", $"{date:S}");
            ConsolePlus.Afficher("    Format .", $"{date:.}");
            ConsolePlus.Afficher("    Format /", $"{date:/}");


            if (date.EstTrèsSpéciale)
            {
                Console.Out.WriteLine("Cette date est très spéciale.");
            }
            else if (date.EstSpeciale)
            {
                Console.Out.WriteLine("Cette date est spéciale.");
            }
            else
            {
                Console.Out.WriteLine("Cette date est sans particularité.");
            }

            //Afficher si jour special ou pas
            ConsolePlus.ColorWriteLine(ConsoleColor.DarkYellow, "\nNoel: {0}", date.EstNoel ? "oui" : "non");

            ConsolePlus.ColorWriteLine(ConsoleColor.DarkYellow, "\nSt-Jean: {0}", date.EstStJean ? "oui" : "non");

            ConsolePlus.ColorWriteLine(ConsoleColor.DarkYellow, "\nJour de l'an: {0}", date.EstJourDeLan ? "oui" : "non");

            Console.Out.WriteLine($"Avant-hier : {date.AvantHier()}");
            Console.Out.WriteLine($"Hier : {date.Hier}");
            Console.Out.WriteLine($"Aujourd'hui : {date.Aujourdhui}");
            Console.Out.WriteLine($"Demain : {date.Demain}");
            Console.Out.WriteLine($"Apres Demain : {date.ApresDemain()}");


            Console.Out.WriteLine(date.Année.EstBissextile()
                ? $"L'année {date.Année} est bisextile"
                : $"L'année {date.Année} n'est pas bisextile");


            ConsolePlus.ColorWriteLine(
                ConsoleColor.DarkCyan,
                "Jour de la semaine: {0}", date.JourDeLaSemaine);

            //Afficher le calendrier
            var calendrier = new Calendrier(date.Année, date.MoisTypé);
//          calendrier[5, 5] = 41;
//            calendrier.Afficher();
            Console.WriteLine("");
            
            //Utiliser ICanlendrier
            Debug.Assert(calendrier is object);
            Debug.Assert(calendrier is Calendrier);
            Debug.Assert(calendrier is ICalendrier);
            ICalendrier ical = default(ICalendrier);
            Debug.Assert(ical == null);
        }

        



        private static Date DemanderDate()
        {
            if (DateUtil.LireDateSeparee("Entrez une date", out Date date))
            {
                ConsolePlus.MessageOK($"\nDate =  {Entexte(date, "-")}");
            }

            return date;
        }
    }

    
    

}