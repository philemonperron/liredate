using System;

namespace Prog2
{
    public static class StringUtil
    {
        /// <summary>
        /// Détermine si une chaine de caractères est purement numérique
        /// donc composée  uniquement de chiffres.
        /// </summary>
        /// <param name="str">La string</param>
        /// <returns>Vrai si numérique</returns>
        public static bool EstNumérique(this string str)
        {
            var valeur = true;
            foreach (var numéro in str)
            {
                if (!char.IsDigit(numéro))
                {
                    valeur = false;
                    break;
                }
            }
            return valeur;
        }

        public static bool EstAlpha(this string str)
        {
            var valeur = true;
            foreach (var numéro in str)
            {
                if (!char.IsLetter(numéro))
                {
                    valeur = false;
                    break;
                }
            }
            return valeur;
        }
    }
}