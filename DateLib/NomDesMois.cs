using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Prog2
{
    public class NomDesMois
    {
        public const string
            Janvier = "janvier",
            Février = "février",
            Mars = "mars",
            Avril = "avril",
            Mai = "mai",
            Juin = "juin",
            Juillet = "juillet",
            Aout = "aout",
            Septembre = "septembre",
            Octobre = "octobre",
            Novembre = "novembre",
            Décembre = "décembre";

        public static readonly List<string> ListeDesMois = new List<string>
        {
            Janvier,
            Février,
            Mars,
            Avril,
            Mai,
            Juin,
            Juillet,
            Aout,
            Septembre,
            Octobre,
            Novembre,
            Décembre,
        };

        public static readonly string[] TableauDesMois = new string[]
        {
            Janvier,
            Février,
            Mars,
            Avril,
            Mai,
            Juin,
            Juillet,
            Aout,
            Septembre,
            Octobre,
            Novembre,
            Décembre,
        };

        /// <summary>
        /// Convertit un mois numerique en string
        /// </summary>
        /// <param name="mois">Le mois, 1 = janvier</param>
        /// <returns>Le nom du mois, null si le nombre n'est pas valide</returns>
        public static string Convertir1(int mois)
        {
            switch (mois)
            {
                case 1:
                    return Janvier;
                case 2:
                    return Février;
                case 3:
                    return Mars;
                case 4:
                    return Avril;
                case 5:
                    return Mai;
                case 6:
                    return Juin;
                case 7:
                    return Juillet;
                case 8:
                    return Aout;
                case 9:
                    return Septembre;
                case 10:
                    return Octobre;
                case 11:
                    return Novembre;
                case 12:
                    return Décembre;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Convertit un mois numerique en string
        /// </summary>
        /// <param name="mois">Le mois, 1 = janvier</param>
        /// <returns>Le nom du mois, null si le nombre n'est pas valide</returns>
        public static string Convertir2(int mois)
        {
            if (mois < 1 || mois > 12)
            {
                return null;
            }

            return TableauDesMois[mois - 1];
        }

        /// <summary>
        /// Convertit un mois numerique en string
        /// </summary>
        /// <param name="mois">Le mois, 1 = janvier</param>
        /// <returns>Le nom du mois, null si le nombre n'est pas valide</returns>
        public static string Convertir3(int mois)
        {
            if (mois < 1 || mois > 12)
            {
                return null;
            }

            return ListeDesMois[mois - 1];
        }

        /// <summary>
        /// Convertit le nom du mois en nombre
        /// Janvier = 1
        /// </summary>
        /// <param name="nom">Le nom du mois</param>
        /// <returns>Le numero du mois, 0 s'il n'existe pas</returns>
        public static int NumeroDuMois1(string nom)
        {
            return ListeDesMois.IndexOf(nom) + 1;
        }

        /// <summary>
        /// Convertit le nom du mois en nombre
        /// Janvier = 1
        /// </summary>
        /// <param name="nom">Le nom du mois</param>
        /// <returns>Le numero du mois, 0 s'il n'existe pas</returns>
        public static int NumeroDuMois2(string nom)
        {
            return Array.IndexOf(TableauDesMois, nom) + 1;
        }

        /// <summary>
        /// Convertit le nom du mois en nombre
        /// Janvier = 1
        /// Case insensitive
        /// </summary>
        /// <param name="nom">Le nom du mois</param>
        /// <returns>Le numero du mois, 0 s'il n'existe pas</returns>
        public static int NumeroDuMois3(string nom)
        {
//          return (Convertir1(ListeDesMois.IndexOf(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(nom.ToLower())) + 1);
            return (ListeDesMois.IndexOf(nom.ToLower()) + 1);
        }

        /// <summary>
        ///  Convertit le nom du mois en nombre
        /// Janvier = 1
        /// Case insensitive
        /// Se fout des accents
        /// </summary>
        /// <param name="nom">Nom du mois</param>
        /// <returns>Le numero du mois ou 0 s'il n'existe pas</returns>
        public static int NumeroDuMois4(string nom)
        {
            var nomSansAccent = nom.ToLower().SansAccent();
            return TableauDesMois
                       .Select(mois => mois.ToLower().SansAccent())
                       .ToList()
                       .FindIndex(m => m == nomSansAccent) + 1;
        }

        /// <summary>
        ///  Convertit le nom du mois en nombre
        /// Janvier = 1
        /// Case insensitive
        /// Se fout des accents
        /// Autocomplete
        /// Returns 0 si c'est ambigue
        /// </summary>
        /// <param name="nom">Nom du mois</param>
        /// <returns>Le numero du mois ou 0 s'il n'existe pas</returns>
        public static int NumeroDuMois5(string nom)
        {
            var nomSansAccent = nom.ToLower().SansAccent();

            var moisTrouve = TableauDesMois
                .Select(mois => mois.ToLower().SansAccent())
                .Where(mois => mois.StartsWith(nomSansAccent))
                .ToList();

            return moisTrouve.Count == 1 ? NumeroDuMois4(moisTrouve[0]) : 0;
        }
    }
}