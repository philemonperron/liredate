using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Prog2
{
    public class Date : object, ICalendrier, IEquatable<Calendrier>, IComparable<Date>, IFormattable, IEquatable<Date>
    {
        // --- Constructeur par defaut ---
        public Date()
        {
            Mois = 1;
            Jour = 1;
            Année = 1;
        }

        // --- Constructeur paramétré ---
        public Date(int année, int mois, int jour)
        {
            Année = année;
            Mois = mois;
            Jour = jour;
        }

        public Date(int année, Mois moisTypé, int jour)
            : this(année, (int) moisTypé, jour)
        {
        }

        /// <summary>
        /// Exemple: new Date("11 septembre 2001").
        /// voir TryParse pour connaître les dates admissibles.
        /// </summary>
        /// <param name="strDate">La date en format texte</param>
        /// <exception cref="System.ArgumentException">date</exception>
        public Date(string strDate)
        {
            if (!TryParse(strDate, out Date date))
                throw new ArgumentException(
                    nameof(strDate), $"## Date invalide: {strDate}");
            Année = date.Année;
            Mois = date.Mois;
            Jour = date.Jour;
        }


        public int Année { get; set; }

        Mois ICalendrier.Mois => _mois1;

        public int[,] Jours { get; }
        public int MoisNumérique { get; }
        public int NbJours { get; }

        public bool Localiser(Date date, out int rangée, out int colonne)
        {
            throw new NotImplementedException();
        }

        //IComparable
        public int CompareTo(Date date)
        {
            return this.ComparerAvec(date);
        }

        public int CompareTo(Calendrier calendrier)
        {
            throw new NotImplementedException();
        }

        public int _mois;
        public int _jour;
        private Mois _mois1;

        public virtual int Jour
        {
            get => _jour;
            set
            {
                if (value < 1 || value > Année.NbJoursDsMois(Mois))

                {
                    throw new ArgumentOutOfRangeException(nameof(Jour), $"## Invalide: {value}");
                }

                _jour = value;
            }
        }


        public virtual int Mois
        {
            get => _mois;
            set
            {
                if (value < 1 || value > 12)
                {
                    throw new ArgumentOutOfRangeException(nameof(Mois), $"## Invalide: {value}");
                }

                _mois = value;
            }
        }

        // --- Propriétés calculables --- 
        public bool EstNoel => Mois == 12 && Jour == 25;
        public bool EstStJean => Mois == 6 && Jour == 24;
        public bool EstJourDeLan => Mois == 1 && Jour == 1;

        /// <summary>
        /// Pour aider a construire une nouvelle date    
        /// </summary>
        /// <param name="annee">Annee</param>
        /// <param name="mois">Mois</param>
        /// <param name="jour">Jour</param>
        /// <returns></returns>
        public static Date New(int annee, int mois, int jour)
        {
            return DateUtil.EstValide(annee, mois, jour) ? new Date {Année = annee, Mois = mois, Jour = jour} : null;
        }

        /// <summary>
        /// Pour aider a construire une nouvelle date    
        /// </summary>
        /// <param name="annee">Annee</param>
        /// <param name="mois">Mois</param>
        /// <param name="jour">Jour</param>
        /// <returns></returns>
        public static Date New(int annee, Mois mois, int jour)
        {
            return New(annee, (int) mois, jour);
        }

        /// <summary>
        /// Donne la date d'avant-hier
        /// </summary>
        /// <returns>la date d'avant-hier/returns>
        public string AvantHier()
        {
            New(
                Année = DateTime.Now.Year,
                Mois = DateTime.Now.Month,
                Jour = DateTime.Now.Day);
            Decrementer();
            Decrementer();
            return EnTexteLong();
        }

        /// <summary>
        /// Donne la date d'hier
        /// </summary>
        /// <value>la date d'hier</value>
        public string Hier
        {
            get
            {
                New(
                    Année = DateTime.Now.Year,
                    Mois = DateTime.Now.Month,
                    Jour = DateTime.Now.Day);
                Decrementer();
                return EnTexteLong();
            }
        }

        /// <summary>
        /// Donne la date d'aujourd'hui
        /// </summary>
        /// <value>la date d'aujourd'hui</value>
        public string Aujourdhui
        {
            get
            {
                New(
                    Année = DateTime.Now.Year,
                    Mois = DateTime.Now.Month,
                    Jour = DateTime.Now.Day);
                return EnTexteLong();
            }
        }

        /// <summary>
        /// Donne la date de demain
        /// </summary>
        /// <value>la date de demain</value>
        public string Demain
        {
            get
            {
                New(
                    Année = DateTime.Now.Year,
                    Mois = DateTime.Now.Month,
                    Jour = DateTime.Now.Day);
                Incrementer();
                return EnTexteLong();
            }
        }

        /// <summary>
        /// Donne la date d'apres demain
        /// </summary>
        /// <returns>la date d'apres demain</returns>
        public string ApresDemain()
        {
            New(
                Année = DateTime.Now.Year,
                Mois = DateTime.Now.Month,
                Jour = DateTime.Now.Day);
            Incrementer();
            Incrementer();
            return EnTexteLong();
        }

        /// <summary>
        /// Pour obtenir la representation textuelle d'une date en format long
        /// Ememple: 11 septembre 2001
        /// </summary>
        /// <returns></returns>
        public string EnTexteLong() => ($"{Jour} {NomDesMois.Convertir1(Mois)} {Année}");


        /// <summary>
        /// Verifie si deux dates sont pareils
        /// </summary>
        /// <param name="date1">Premiere date</param>
        /// <param name="date2">Deuxieme date</param>
        /// <returns>Vrai si egales</returns>
        public static bool SontEgales(Date date1, Date date2)
        {
            return date1.Année == date2.Année && date1.Mois == date2.Mois && date1.Jour == date2.Jour;
        }

        /// <summary>
        /// Pour cloner une date en modifiant certains attributs au besoin.
        /// </summary>
        /// <param name="date">date à cloner</param>
        /// <param name="année">annee modifiée au besoin</param>
        /// <param name="mois">Mois modifié au besoin</param>
        /// <param name="jour">Jour modifié au besoin</param>
        /// <returns></returns>
        public Date Cloner( /*Date this,*/ int année = 0, int mois = 0, int jour = 0)
        {
            var d = new Date
                {Année = this.Année, Mois = Mois, Jour = Jour};
            if (année != 0) d.Année = année;
            if (Mois != 0) d.Mois = Mois;
            if (Jour != 0) d.Jour = Jour;
            return d;
        }

        /// <summary>
        /// Pour savoir si une date est speciale.
        /// Une date est speciale si le mois et le jour sont identiques
        /// </summary>
        /// <value>Vrai si speciale</value>
        public bool EstSpeciale => Jour == Mois;

        /// <summary>
        /// Pour savoir si une date est tres speciale.
        /// Une date est speciale si l'annee, le mois et le jour sont identiques
        /// </summary>
        /// <value>Vrai si tres speciale</value>
        public bool EstTrèsSpéciale => Année % 100 == Jour && Mois == Jour;

        /// <summary>
        /// Modifie la date pour la mettre a jour d'aujourd'hui
        /// </summary>
        ///<returns>La date modifiée</returns>
        public Date MettreAJour( /*Date this*/)
        {
            Jour = DateTime.Today.Day;
            Mois = DateTime.Today.Month;
            Année = DateTime.Today.Year;
            return this;
        }

        /// <summary>
        /// Incremente la date de plusieurs jours
        /// </summary>
        /// <param name="increment">Nombre de jours a incrementer</param>
        public virtual Date Incrementer( /*Date this*/ int increment = 1)
        {
            for (var i = 0; i < increment; i++)
            {
                if (Année.NbJoursDsMois(Mois) >= Jour + 1)
                {
                    Jour++;
                }
                else
                {
                    Jour = Jour + 1 - Année.NbJoursDsMois(Mois);
                    if (Mois == 12)
                    {
                        Mois = 1;
                        Année++;
                    }
                    else
                    {
                        Mois++;
                    }
                }
            }

            return this;
        }

        /// <summary>
        /// Decremente la date de plusieurs jours
        /// </summary>
        /// <param name="decrement">Nombre de jours a decrementer</param>
        public virtual Date Decrementer( /*Date this*/ int decrement = 1)
        {
            for (var i = 0; i < decrement; i++)
            {
                if (Jour != 1)
                {
                    Jour -= 1;
                }
                else
                {
                    if (Mois != 1)
                    {
                        Mois--;
                    }
                    else
                    {
                        Mois = 12;
                        Année--;
                    }

                    Jour = Année.NbJoursDsMois(Mois);
                }
            }

            return this;
        }

        /// <summary>
        /// Retourne le mois sous forme de type Mois.
        /// </summary>
        /// <value>Mois typé en Mois</value>
        public Mois MoisTypé
        {
            get { return (Mois) Mois; }
            set { Mois = (int) value; }
        }

        public override bool Equals(object obj)
        {
            var other = (Date) obj;
            return Année == other.Année && Mois == other.Mois && Jour == other.Jour;
        }
        
        public bool Equals(Date obj)
        {
            
            var other = (Date) obj;
            return Année == other.Année && Mois == other.Mois && Jour == other.Jour;
        }

        //IFormattable
        public string ToString(string format, IFormatProvider formatProvider)
        {
            format = format ?? "G";

            switch (format.ToUpperInvariant())
            {
                case "G":
                case "":
                case "-":
                    return this.ToString();
                case "L":
                    return this.EnTexteLong();
                case "S":
                    return DateUtil.Entexte(this, " ");
                case ".":
                    return DateUtil.Entexte(this, ".");
                case "/":
                    return DateUtil.Entexte(this, "/");
                default:
                    throw new FormatException($"Le format {format} n'est pas supporté pour une Date.");
            }
        }

        public static bool TrySplitDate(string strDate, out string strAnnée, out string strMois, out string strJour)
        {
            // --- Fonction locales ---
            bool EstAnnée(string str) => str.EstNumérique() && str.Length >= 3;
            bool EstJour(string str) => str.EstNumérique() && str.Length <= 2;
            bool EstMois(string str) => str.EstNumérique() && str.Length <= 2;
            bool EstMoisLettre(string str) => NomDesMois.NumeroDuMois5(str) != 0;


            var dateSeparee = strDate
                .Replace("(", "")
                .Replace(")", "")
                .Split(' ', '-', '/', '.', ',', ':', ';')
                .Select(s => s.Trim())
                .Where(s => !string.IsNullOrEmpty(s))
                .ToList();

            var dateCopie = dateSeparee.ToList();

            strMois = dateSeparee.FirstOrDefault(EstMoisLettre);
            dateSeparee.Remove(strMois);
            if (string.IsNullOrEmpty(strMois))
            {
                strMois = dateSeparee.FirstOrDefault(EstMois);
                dateSeparee.Remove(strMois);
            }

            strAnnée = dateSeparee.FirstOrDefault(EstAnnée);
            dateSeparee.Remove(strAnnée);

            strJour = dateSeparee.FirstOrDefault(EstJour);
            dateSeparee.Remove(strJour);


            if (dateCopie.Count != 3 ||
                string.IsNullOrEmpty(strAnnée) || string.IsNullOrEmpty(strMois) || string.IsNullOrEmpty(strJour) ||
                (dateCopie.IndexOf(strMois) == 0 && !EstMoisLettre(strMois)))
            {
                strJour = "";
                strMois = "";
                strAnnée = "";
                return false;
            }

            return true;
        }

        public static bool TryParse(string strDate, out Date date)
        {
            var dateEstSeparee = TrySplitDate(strDate, out var strAnnee, out var strMois, out var strJour);
            if (dateEstSeparee)
            {
                var année = int.Parse(strAnnee);
                DateUtil.TryParseMois(strMois, out var mois);
                var jour = int.Parse(strJour);

                date = New(année, mois, jour);
                return date != null;
            }

            date = null;
            return false;
        }

        /// <summary>
        /// Le jour de l'an
        /// ex: 1er janvier = 1
        ///     1 fevrier = 32
        /// </summary>
        public virtual int JourDeLAnnée
        {
            get
            {
                var jour = Jour;
                for (var i = 1; i < Mois; i++)
                {
                    jour += Année.NbJoursDsMois(i);
                }


                return jour;
            }
            set
            {
                if (value < 1 || value > 366)
                    throw new ArgumentOutOfRangeException(
                        nameof(JourDeLAnnée), $"## Invalide: {value}");

                Mois = 1;
                Jour = 1;
                Incrementer(value - 1);
            }
        }

        /// <summary>
        /// Compare cette date-ci (this) avec une autre date
        /// </summary>
        /// <param name="autre">L'autre date</param>
        /// <returns>
        ///     0 si egaux
        ///     1 si 1 > 2
        ///    -1 si 1 < 2
        /// </returns>
        public int ComparerAvec(Date autre)
        {
            if (Année == autre.Année)
            {
                if (Mois == autre.Mois)
                {
                    if (Jour == autre.Jour)
                    {
                        return 0;
                    }

                    if (Jour > autre.Jour)
                    {
                        return 1;
                    }

                    return -1;
                }

                if (Mois > autre.Mois)
                {
                    return 1;
                }

                if (Mois < autre.Mois)
                {
                    return -1;
                }

                return Année > autre.Année ? 1 : -1;
            }

            return Année > autre.Année ? 1 : -1;
        }

        /// <summary>
        /// Génère une date aléatoire comprise entre les années spécifiées.
        /// La distribution est uniforme et toutes les date sont possibles.
        /// </summary>
        /// <param name="random">Générateur utilisé pour générer la date</param>
        /// <param name="anneeMin">Année min</param>
        /// <param name="anneeMax">Année max</param>
        /// <returns>Une nouvelle date aléatoire.</returns>
        /// <exception cref="System.ArgumentException">Max doit etre plus grand que min</exception>
        public static Date Aléatoire(Random random, int anneeMin, int anneeMax)
        {
            if (anneeMax < anneeMin)
                throw new ArgumentException("## Max doit être plus grand que min");

            var année = random.Next(anneeMin, anneeMax + 1);
            var jourDeLAnnée = random.Next(1, année.EstBissextile() ? 367 : 366);

            return new Date {Année = année, JourDeLAnnée = jourDeLAnnée};
        }

        /// <summary>
        /// Calcule le nombre de jours qui separe deux date, positif ou negatif.
        /// Ex:
        ///     Date.Demain.Moins(Date.Hier) => 2
        ///     Date.Hier.Moins(Date.Demain) => -2
        /// </summary>
        /// <param name="autre">L'autre date</param>
        /// <returns>La difference</returns>
        public int Moins(Date autre)
        {
            return 0;
        }


        public bool Equals(Calendrier other)
        {
            throw new NotImplementedException();
        }


        

        public override string ToString()
        {
            return DateUtil.Entexte(this);
        }

        public JourDeLaSemaine JourDeLaSemaine
        {
            get
            {
                int m = Mois;
                int K = Année % 100;
                int J = Année / 100;
                if (Mois < 3)
                {
                    K = (Année - 1) % 100;
                    J = (Année - 1) / 100;
                }

                if (m == 1)
                    m = 13;
                if (m == 2)
                    m = 14;
                int jourDeLaSemaine =
                    ((Jour + (13 * (m + 1)) / 5 + K + (K / 4
                                                       + (J / 4) + 5 * J)) % 7);

                return (JourDeLaSemaine) ((jourDeLaSemaine + 5) % 7);
            }
        }

        /// <summary>
        /// Pour cloner une date
        /// </summary>
        /// <returns>Le clone</returns>
        public Date Dupliquer() => MemberwiseClone() as Date;
    }
}