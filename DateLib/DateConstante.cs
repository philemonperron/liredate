using System;

namespace Prog2
{
    public class DateConstante : Date
    {
        public DateConstante(int année, int mois, int jour) : base(année, mois, jour)
        {
        }

//        public override int Mois
//        {
//            get => base.Mois;
//            set => throw OperationInvalide();
//        }

        //Attribut dervant de cache
        private int JourDeLAnnéeCaché = 0;
        
        public override int JourDeLAnnée
        {
            get
            {
                if (JourDeLAnnéeCaché == 0)
                    JourDeLAnnéeCaché = base.JourDeLAnnée;
                return JourDeLAnnéeCaché;
                
            }
            set => throw OperationInvalide();
        }

        public override Date Incrementer(int increment = 1)
        {
            throw OperationInvalide();
        }
        
        public override Date Decrementer(int increment = 1)
        {
            throw OperationInvalide();
        }
        

        private Exception OperationInvalide()
        {
//            return new InvalidOperationException("## Date constante non modifiable");
            return new DateConstanteException();
        }
        
       
    }
}