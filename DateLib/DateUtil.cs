using System;
using System.Linq;

namespace Prog2
{
    public static class DateUtil
    {
        //Attributs
        public static readonly Date dateAttentatWTC = new Date(2001, 09, 11);
        public static readonly Date dateDecesMJ = Date.New(2009, 06, 25);
        public static readonly Date dateExplosionNC = Date.New(1986, 01, 28);
        public const int dummy = 10;

        /// <summary>
        /// Converti la date en beau texte aaaa-mm-jj
        /// </summary>
        /// <param name="date">Objet date</param>
        /// <param name="separateur">Separateur entre les nombres</param>
        /// <returns>Date avec le separateur voulu</returns>
        public static string Entexte(Date date, string separateur = "-")
        {
            var dateTexte = ($"{date.Année:D4}{separateur}{date.Mois:00}{separateur}{date.Jour:00}");
            return dateTexte;
        }


        /// <summary>
        /// Demande l'annee, le mois et le jour
        /// </summary>
        /// <param name="propriété">Texte ecrit</param>
        /// <param name="date">Objet avec annee, mois et jour</param>
        /// <returns></returns>
        public static bool LireDateSeparee(string propriété, out Date date)
        {
            ConsolePlus.LireEntier("Année", " ", int.MinValue, int.MaxValue,
                out var annee);
            LireMois("Mois", " ", out var mois);
            ConsolePlus.LireEntier("Jour", " ", 1, 31,
                out var jour);

            date = new Date
            {
                Année = annee,
                Mois = mois,
                Jour = jour
            };

            return true;
        }


        /// <summary>
        /// Determine si une annee est bisextile
        /// </summary>
        /// <param name="annee">Annee dont il faut trouver si elle est bisextile</param>
        /// <returns>True si bisextile</returns>
        public static bool EstBissextile(this int annee)
        {
            return annee % 4 == 0 && (annee % 100 != 0 || annee % 400 == 0);
        }

        /// <summary>
        /// Determine le nombre de jours dans un mois
        /// </summary>
        /// <param name="annee">Annee</param>
        /// <param name="mois">Mois</param>
        /// <returns>Le nombre de jours dans le mois</returns>
        public static int NbJoursDsMois(this int annee, int mois)
        {
            switch (mois)
            {
                case 2 when annee.EstBissextile():
                    return 29;
                case 2:
                    return 28;
                default:
                    switch (mois)
                    {
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 8:
                        case 10:
                        case 12:
                            return 31;
                        case 4:
                        case 6:
                        case 9:
                        case 11:
                            return 30;
                        default:
                            return 0;
                    }
            }
        }

        /// <summary>
        /// Determine si la combinaison (annee, mois et jour) constitue une date valide
        /// </summary>
        /// <param name="annee">Annee</param>
        /// <param name="mois">Mois</param>
        /// <param name="jour">Jour</param>
        /// <returns></returns>
        public static bool EstValide(int annee, int mois, int jour)
        {
            return (mois <= 12 && mois >= 1) && (jour <= NbJoursDsMois(annee, mois) && jour >= 1);
        }

        public static bool TryParseMois(string strMois, out int mois)
        {
            var numeroDuMois = NuméroDuMois(strMois);

            if (numeroDuMois != 0)
            {
                mois = numeroDuMois;
                return true;
            }

            var estUnChiffre = int.TryParse(strMois, out mois);

            return estUnChiffre && mois > 0 && mois <= 12;
        }

        /// <summary>
        /// Lit un mois sur la console.
        /// 1, 01, jan, JAN ou janvier sont equivalents
        /// </summary>
        /// <param name="propriete">Nom de la propriete a afficher</param>
        /// <param name="defaut">Valeur par defaut (null si aucune)</param>
        /// <param name="mois">Vrai si ca marche</param>
        /// <returns></returns>
        public static bool LireMois(string propriete, string defaut, out int mois)
            => TryParseMois(ConsolePlus.Demander(propriete, defaut), out mois);


        /// <summary>
        /// Obtient le numero correspondant au nom du mois spécifié.
        /// Les accents et la casse ne compent pas pour la recherche.
        /// On peut spécifier uniquement le début du mois,
        /// mais il ne doit pas y avoir d'ambiguité.
        /// Ex: NuméroDuMois("FEV") = 2
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public static int NuméroDuMois(string nom)
        {
            var nomSansAccent = nom.ToLower().SansAccent();

            var moisTrouve = Enum.GetNames(typeof(Mois))
                .Select(mois => mois.ToLower().SansAccent())
                .Where(mois => mois.StartsWith(nomSansAccent))
                .ToList();

            return moisTrouve.Count == 1 ? NomDesMois.NumeroDuMois4(moisTrouve[0]) : 0;
        }

        /// <summary>
        /// Permet de lire une date sur la console.
        ///
        ///     Ex:
        ///         LireDAte("Date de naissance", "1800-01-01", Date.Aujourdhui(), out Date date)
        /// </summary>
        /// <param name="propriété">Nom de la propriete</param>
        /// <param name="min">Valeur min permise</param>
        /// <param name="max">Valeur max permise</param>
        /// <param name="date">Valeur lue</param>
        /// <param name="defaut">valeur par defaut</param>
        /// <returns>Faux si l'entree n'est pas lisible ou si l'entree n'est pas valdie</returns>
        public static bool LireDate(string propriété, string min, string max, out Date date, string defaut = null)
        {
            ConsolePlus.Colorwrite(ConsoleColor.Cyan, "Date à analyser: ");
            var dateLue = Console.ReadLine();


            var dateValideMin = Date.TryParse(min, out var dateMin);
            var dateValideMax = Date.TryParse(max, out var dateMax);
            var dateValideLue = Date.TryParse(dateLue, out date);


            if (!dateValideLue || !dateValideMin || !dateValideMax)
            {
                ConsolePlus.MessageErreur("La date doit être valide.");
                return false;
            }

            if (date.ComparerAvec(dateMax) == 1)
            {
                ConsolePlus.MessageErreur($"La date doit être plus petite ou égale à {dateMax}");
                return false;
            }

            if (date.ComparerAvec(dateMin) == -1)
            {
                ConsolePlus.MessageErreur($"La date doit être plus grande ou égale à {dateMin}");
                return false;
            }

            return true;
        }

        public static string EnTexte(object obj)
        {
            switch (obj)
            {
                case Date date:
                    return date.EnTexteLong();
                case bool b:
                    return b.OuiNon();
                case null:
                    return "null";
                case Calendrier calendrier:
                    return $"Calendrier {calendrier.Mois} {calendrier.Année}";
                default:
                    return obj.ToString();
            }
        }

        
    }
}