using System;

namespace Prog2
{
    public class Calendrier : ICalendrier
    {
        public int this[int rangée, int colonne] => Jours[rangée, colonne];

        public const int NbRangées = 6;
        public const int NbColonnes = 7;

        public int Année { get; }
        public Mois Mois { get; }
        public int[,] Jours { get; }


        public override bool Equals(object obj)
            => obj is Calendrier calendrier
               && this.Equals(calendrier);


        public bool Equals(Calendrier calendrier)
            => Année == calendrier.Année
               && Mois == calendrier.Mois;


        public override int GetHashCode()
            => Année * 100 + MoisNumérique;
        
        public int CompareTo(ICalendrier autre)
        {
            if (this.Année == autre.Année)
            {
                if (this.Mois == autre.Mois)
                {
                    return 0;
                }
                else
                    return (this.Mois > autre.Mois) ? 1 : -1;
            }
            else
                return (this.Année > autre.Année) ? 1 : -1;
        }
        
        public int CompareTo(Calendrier autre)
        {
                     if (this.Année == autre.Année)
                     {
                         if (this.Mois == autre.Mois)
                         {
                             return 0;
                         }
                         else
                             return (this.Mois > autre.Mois) ? 1 : -1;
                     }
                     else
                         return (this.Année > autre.Année) ? 1 : -1;
        }


        public Calendrier(int année, Mois mois)
        {
            if (année < 1582 || année > 9999)
                throw new ArgumentOutOfRangeException(nameof(année), $"## Invalide: {année}");
            if ((int) mois < 1 || (int) mois > 12)
                throw new ArgumentOutOfRangeException(nameof(mois), $"## Invalide: {mois}");

            this.Année = année;
            this.Mois = mois;
            this.Jours = new int[NbRangées, NbColonnes];
            RemplirTableau();
        }


        private void RemplirTableau()
        {
            Date dateTest = new Date(this.Année, this.Mois, 1);
            int journée = 1;
            for (int rangée = 0; rangée < NbRangées; rangée++)
            {
                for (int colonne = rangée == 0 ? (int) dateTest.JourDeLaSemaine % NbColonnes : 0; colonne < NbColonnes; colonne++)
                {
                    if (journée > DateUtil.NbJoursDsMois(this.Année, (int) this.Mois))
                        Jours[rangée, colonne] = 0;
                    else
                    {
                        Jours[rangée, colonne] = journée;
                        journée++;
                    }
                }
            }
        }


        /// <summary>
        /// Localise la position (rangée et colonne) d'une date dans le calendrier
        /// </summary>
        /// <param name="date">La date à localiser</param>
        /// <param name="rangée">La rangée trouvée</param>
        /// <param name="colonne">La colonne trouvée</param>
        /// <returns>Vrai si la date se trouve dans ce calendrier</returns>
        public bool Localiser(Date date, out int rangée, out int colonne)
        {
            if (date == null || date.Mois != (int) this.Mois)
            {
                colonne = 0;
                rangée = 0;
                return false;
            }

            Date dateTest = new Date(this.Année, this.Mois, 1);
            int journée = 1;
            for (rangée = 0; rangée < NbRangées; rangée++)
            {
                for (colonne = rangée == 0 ? (int) dateTest.JourDeLaSemaine % NbColonnes : 0; colonne < NbColonnes; colonne++)
                {
                    if (journée == date.Jour)
                    {
                        return true;
                    }
                    else
                    {
                        journée++;
                    }
                }
            }

            colonne = 0;
            rangée = 0;
            return false;
        }

        public int NbJours => DateUtil.NbJoursDsMois(Année, (int) Mois);


        public int MoisNumérique => (int) Mois;

        public static bool operator <(Calendrier calendrier1, Calendrier calendrier2)
            => calendrier1.CompareTo(calendrier2) == -1;

        public static bool operator >(Calendrier calendrier1, Calendrier calendrier2)
        {
           return calendrier1.CompareTo(calendrier2) == 1;
        }


        public override string ToString()
        {
            return DateUtil.EnTexte(this);
        }
    }
}