using System;

namespace Prog2
{
    public interface ICalendrier : IEquatable<Calendrier>, IComparable<Calendrier>
    {
        int Année { get; }
        Mois Mois { get; }
        int[,] Jours { get; }
        int MoisNumérique { get; }
        int NbJours { get; }

        /// <summary>
        /// Localise la position (rangée et colonne) d'une date dans le calendrier
        /// </summary>
        /// <param name="date">La date à localiser</param>
        /// <param name="rangée">La rangée trouvée</param>
        /// <param name="colonne">La colonne trouvée</param>
        /// <returns>Vrai si la date se trouve dans ce calendrier</returns>
        bool Localiser(Date date, out int rangée, out int colonne);
    }
}