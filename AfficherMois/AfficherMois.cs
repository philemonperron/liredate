﻿using System;
using System.Diagnostics;
using Prog2;
using static System.ConsoleColor;
using static Prog2.ConsolePlus;

namespace AfficherMois
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "AfficherMois";

            // --- Conversion entier/enum ---
            ColorWriteLine(White, "\nConversion entier-enum:");
            ColorWriteLine(Magenta, $"{Mois.Janvier,8} == {(int) Mois.Janvier}");
            ColorWriteLine(DarkYellow, $"{Mois.Decembre,8} == {(int) Mois.Decembre}");
            ColorWriteLine(DarkGreen, $"{(Mois) 4,8} == {4}");
            Debug.Assert((int) Mois.Mars == 3);
            Debug.Assert(Mois.Juin == (Mois) 6);

            // --- Addition et soustraction ---
            ColorWriteLine(White, "\nAddition et soustraction:");
            ColorWriteLine(DarkCyan, $"{Mois.Juin} + 1 == {Mois.Juin + 1}");
            ColorWriteLine(DarkCyan, $"{Mois.Aout} + 1 == {Mois.Aout + 1}");
            Mois mois = Mois.Decembre;
            mois -= 8;
            Debug.Assert(mois == Mois.Avril);
            ColorWriteLine(Blue, $"{Mois.Decembre} - 8 == {mois}");
            ColorWriteLine(DarkGreen, $"{Mois.Aout} - {Mois.Avril} == {Mois.Aout - Mois.Avril}");
            
            // --- Comparaisons ---
            ColorWriteLine(White, "\nComparaisons");
            ColorWriteLine(DarkMagenta, $"({Mois.Mars} == {Mois.Avril}) == {Mois.Mars == Mois.Avril}");
            ColorWriteLine(DarkMagenta, $"({Mois.Mars} != {Mois.Avril}) == {Mois.Mars != Mois.Avril}");
            ColorWriteLine(DarkMagenta, $"{Mois.Mars} < {Mois.Avril} == {Mois.Mars < Mois.Avril}");
            ColorWriteLine(DarkMagenta, $"{Mois.Mars} > {Mois.Avril} == {Mois.Mars > Mois.Avril}");
            
            // --- Enumeration --- 
            Colorwrite(White, "\nLes mois sont:");
            for (Mois m = Mois.Janvier; m <= Mois.Decembre; m++)
            {
                Colorwrite(DarkRed, $"{m} ");
            }
            Colorwrite(DarkGray, "\nLes mois sont:");
            foreach (Mois m  in Enum.GetValues(typeof(Mois)))
            {
                Colorwrite(DarkYellow, $"{m} ");
            }
            
            // ---Étrangetés --- 
            ColorWriteLine(White, "\n\nBizarre... ");
            ColorWriteLine(DarkCyan, $"{Mois.Decembre} + 1 == {Mois.Decembre + 1}");
            ColorWriteLine(DarkCyan, $"{Mois.Decembre} + 2 == {Mois.Decembre + 2}");
            ColorWriteLine(DarkCyan, $"default(mois) == {default(Mois)}");
            ColorWriteLine(DarkCyan, $"default(mois) + 1 == {default(Mois) + 1}");
        }
    }
}