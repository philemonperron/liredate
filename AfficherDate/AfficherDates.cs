﻿using System;
using System.IO;
using System.Linq;
using Prog2;
using static Prog2.DateUtil;
using static Prog2.ConsolePlus;

namespace AfficherDate
{
    static class AfficherDates
    {
        static void Main(string[] args)
        {
            string nomFichier = "MonFichier.txt";

            try
            {
                MainSwitch(args, ref nomFichier);
            }
            catch (FileNotFoundException exception)
            {
                MessageErreur($"Le fichier '{nomFichier}' est introuvable");
                ColorWriteLine(ConsoleColor.Magenta, "\nMessage:\n{0}", exception.Message);
                ColorWriteLine(ConsoleColor.DarkYellow, "\nSTACK TRACE:\n{0}", exception.StackTrace);
            }
            // --- Catchall ---
            catch (Exception ex)
#if DEBUG
                when (!ex.Message.StartsWith("##"))
#endif
            {
                MessageErreur(ex.Message);
            }
        }

        private static void MainSwitch(string[] args, ref string nomFichier)
        {
            switch (args.Length)
            {
                case 0:
                    // --- Aucun argument, on se rabat sur l'ancien programme ---
                    AncienMain();
                    break;

                case 1:
                    // --- On affiche le contenu du fichier spécifié en argument ---
                    nomFichier = args[0];
                    LireFichier(nomFichier);
                    break;

                default:
                    // --- Message d'usage --- 
                    ColorWriteLine(ConsoleColor.DarkYellow, "USAGE: AfficherDate [NomFichier]");
                    break;
            }
        }

        private static void LireFichier(string nomFichier)
        {
            var lignes = File.ReadLines(nomFichier)
                .Where(ligne => !string.IsNullOrEmpty(ligne))
                .ToList();

            lignes.ForEach(AfficherLigne);

            var rowsHtml = lignes.Select(CreerRow);
            var htmlDeBase = File.ReadAllText("date.html");
            var html = htmlDeBase.Replace("EVENEMENTS", string.Join("", rowsHtml));

            var nomFichierHtml = nomFichier + ".html";
            File.WriteAllText(nomFichierHtml, html);
            MessageOK($"\nFichier HTML généré: {nomFichierHtml}");
        }

        private static string CreerRow(string ligne)
        {
            ValiderLigne(ligne, out var date, out var message, out var messageValide, out var dateValide);
            return messageValide && dateValide ? $"<tr><td>{message}</td><td>{date.Année}</td><td>{date.Mois}</td><td>{date.Jour}</td></tr>" : "";
        }

        private static void AfficherLigne(string ligne)
        {
            ValiderLigne(ligne, out var date, out var message, out var messageValide, out var dateValide);
            EcrireConsole(ligne, dateValide, messageValide, date, message);
        }

        private static void ValiderLigne(string ligne, out Date date, out string message, out bool messageValide, out bool dateValide)
        {
            var ligneSeparee = ligne.Split(':');

            var strDate = ligneSeparee[0];
            dateValide = Date.TryParse(strDate, out date);

            message = ligneSeparee.Length == 2 ? ligneSeparee[1] : " ???";
            messageValide = ligneSeparee.Length <= 2;
        }

        private static void EcrireConsole(string ligne, bool dateValide, bool messageValide, Date date, string message)
        {
            if (dateValide && messageValide)
            {
                Colorwrite(ConsoleColor.Cyan, Entexte(date) + ":");
                ColorWriteLine(ConsoleColor.DarkYellow, message);
            }
            else if (!dateValide)
            {
                ColorWriteLine(ConsoleColor.DarkRed, "Date invalide: {0}", message);
            }
            else
            {
                ColorWriteLine(ConsoleColor.DarkRed, "Format erroné: {0}", ligne);
            }
        }


        private static void AncienMain()
        {
            Console.Title = "AfficherDates";
            ColorWriteLine(ConsoleColor.Green, "\nAttentat du WTC: {0}",
                Entexte(dateAttentatWTC, "-"));
            ColorWriteLine(ConsoleColor.Yellow, "\nMort de MJ: {0}",
                Entexte(dateDecesMJ, "/"));
            ColorWriteLine(ConsoleColor.Cyan, "\nExplosion de la NC: {0}",
                Entexte(dateExplosionNC, "."));
            ColorWriteLine(ConsoleColor.DarkYellow,"\nDate par defaut: {0}", Entexte(new Date()));
            ColorWriteLine(ConsoleColor.DarkYellow,"Jour 100 de 2019 {0}", new Date{Année = 2019, JourDeLAnnée = 100}.EnTexteLong());
            Poursuivre();
        }
    }
}