using System;
using System.Text;
using static System.Console;

namespace Prog2

{
    public static class ConsolePlus
    {
        /// <summary>
        /// Précise si les messages d'erreur ou ok sont bloquants ou pas.
        /// Un message bloquant demande à l'utilisateur d'appuyer sur une touche avant
        /// de poursuivre.
        /// </summary>
        public static bool Bloquant = false;

        /// <summary>
        /// Write mais avec une couleur
        /// </summary>
        /// <param name="color">Couleur du message</param>
        /// <param name="message">Message a afficher</param>
        public static void Colorwrite(ConsoleColor color, string message, params object[] args)
        {
            var couleurInitiale = ForegroundColor;
            ForegroundColor = color;
            try
            {
                Write(message, args);
            }
            finally
            {
                ForegroundColor = couleurInitiale;
            }
        }


        /// <summary>
        /// WriteLine mais avec une couleur choisie
        /// </summary>
        /// <param name="color">Couleur du message</param>
        /// <param name="message">Message a afficher</param>
        public static void ColorWriteLine(ConsoleColor color, string message, params object[] args)
        {
            var couleurInitiale = ForegroundColor;
            ForegroundColor = color;
            try
            {
                WriteLine(message, args);
            }
            finally
            {
                ForegroundColor = couleurInitiale;
            }
        }

        /// <summary>
        /// Demande à l'utilisateur d'entrer une donné avec une valeur par défaut s'il n'entre rien
        /// </summary>
        /// <param name="propriete"></param>
        /// <param name="defaut"></param>
        /// <returns></returns>
        public static string Demander(string propriete, string defaut)
        {
            if (defaut == null)
                defaut = " ";
            if (defaut != " ")
                propriete += $"[{defaut}]";
            Colorwrite(ConsoleColor.Cyan, $"{propriete}: ");
            var reponse = ReadLine().Trim();
            if (reponse == " ")
                return defaut;
            return reponse;
        }

        /// <summary>
        /// Comme LireEntier mais de demande pas de specifier un max ni un min
        /// </summary>
        /// <param name="propriete">Nom de la propriete</param>
        /// <param name="defaut">Valeur par defaut</param>
        /// <param name="entier">Valeur lue</param>
        /// <returns></returns>
        public static bool LireEntier(string propriete, string defaut, out int entier)
        {
            return LireEntier(propriete, defaut, int.MinValue, Int32.MaxValue, out entier);
        }

        /// <summary>
        /// Permet de lire un entier sur la console
        /// </summary>
        /// <param name="propriete">Nom de la propriete</param>
        /// <param name="defaut">Valeur par defaut</param>
        /// <param name="min">Valeur minimale permise</param>
        /// <param name="max">Valeur maximale permise</param>
        /// <param name="entier">Valeur lue</param>
        /// <returns>Faux si une erreur se produit</returns>
        public static bool LireEntier(string propriete, string defaut, int min, int max, out int entier)
        {
            if (int.TryParse(Demander(propriete, defaut), out entier))
            {
                if (entier < min)
                    MessageErreur($"Le nombre doit être plus grand ou égale à {min}");
                else if (entier > max)
                    MessageErreur($"Le nombre doit être plus petit ou égale à {max}");
                else
                    return true;
            }
            else
                MessageErreur("Vous devez entrer un nombre entier.");

            return false;
        }

        /// <summary>
        /// Écrit un message d'erreur en rouge
        /// </summary>
        /// <param name="message">Message à afficher</param>
        public static void MessageErreur(string message, params object[] args)
        {
            ColorWriteLine(ConsoleColor.Red, message);
            if (Bloquant) Poursuivre();
        }

        /// <summary>
        /// Ecrit un message de confirmation en vert
        /// </summary>
        /// <param name="message">Message à afficher</param>
        public static void MessageOK(string message, params object[] args)
        {
            ColorWriteLine(ConsoleColor.Green, message);
            if (Bloquant) Poursuivre();
        }

        /// <summary>
        /// Demande a l'utilisateur d'appuyer sur une touche avant de continuer
        /// </summary>
        public static void Poursuivre()
        {
            Colorwrite(ConsoleColor.Gray, "\nAppuyer sur une touche pour poursuivre...");
            ReadKey(true);
            WriteLine();
        }

        /// <summary>
        /// Supprime les accents.
        /// </summary>
        /// <param name="str">La string dont il faut enlever les accents</param>
        /// <returns>Une nouvelle chaien sans accent</returns>
        public static string SansAccent(this string str)
        {
            return Encoding.ASCII.GetString(Encoding.GetEncoding(1251).GetBytes(str));
        }

        public static bool LireBooleen(string propriete, out bool booleen, string defaut = null)
        {
            if (Demander(propriete, defaut).TryParseBool(out booleen))
                return true;

            else
            {
                MessageErreur("Il faut entrer oui/non ou o/n ou en anglais");
                return false;    
            }
            
        }

        public static void Poursuivre(string message = null)
        {
            message = message == null ? "Appuyer sur une touche pour poursuivre..." : message;
            ColorWriteLine(ConsoleColor.Gray, $"\n{message}");
            ReadKey(true);
            WriteLine();
        }

        public static void Afficher(string propriete,
            object valeur,
            int offset = 0,
            ConsoleColor couleurValeur = 0,
            ConsoleColor couleurPropriete = 0)
        {
            propriete += ": ";
            couleurPropriete = couleurPropriete == 0 ? ConsoleColor.Cyan : couleurPropriete;
//            Console.Write(couleurPropriete, $"{{0,{offset}}}", propriete);
            couleurValeur = couleurValeur == 0 ? ForegroundColor : couleurValeur;
            Out.WriteLine($"{propriete} = {valeur}");
        }
    }
}