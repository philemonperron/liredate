using System.Linq.Expressions;

namespace Prog2
{
    public static class StringUtil
    {
        public static bool TryParseBool(this string strBool, out bool booleen)
        {
            switch (strBool.ToLower().Trim())
            {
                case "oui":
                case "vrai":
                case "true":
                case "yes":
                case "o":
                case "v":
                case "t":
                case "y":
                case "1":
                    booleen = true;
                    return true;
                
                case "non":
                case "faux":
                case "false":
                case "no":
                case "n":
                case "f":
                case "0":
                    booleen = false;
                    return false;
                    
                default:
                    booleen = false;
                    return false;
            }
        }
    }
}