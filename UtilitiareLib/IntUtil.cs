namespace Prog2
{
    public static class IntUtil
    {
        /// <summary>
        /// Caompare 2 entiers
        /// </summary>
        /// <param name="ceci">1er nombre</param>
        /// <param name="cela">2eme nombre</param>
        /// <returns>
        ///     0 si egaux
        ///     1 si 1 > 2
        ///     2 si 1 < 2
        /// </returns>
        public static int ComparerAvec(this int ceci, int cela)
        {
            if (ceci > cela)
            {
                return 1;
            }

            if (ceci < cela)
            {
                return -1;
            }

            return 0;
        }
    }
}