namespace Prog2
{
    public static class BoolUtil
    {
        public static string OuiNon(this bool b)
            => b ? "oui" : "non";

        public static string VraiFaux(this bool b)
            => b ? "vrai" : "faux";

    }
}