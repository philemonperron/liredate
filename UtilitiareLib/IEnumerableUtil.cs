﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Prog2.ConsolePlus;


namespace Prog2
{
    public static class IEnumerableUtil
    {
        /// <summary>
        /// Convertit un énumérable en texte.
        /// </summary>
        /// <typeparam name="T">Type générique quelconque</typeparam>
        /// <param name="elems">Les éléments énumérable</param>
        /// <param name="séparateur">Le séparateur à insérer entre les éléments</param>
        /// <param name="texteAvant">Texte à ajouter devant chaque élément</param>
        /// <param name="texteAprès">Texte à ajouter après chaque élément</param>
        /// <returns>L'énumérable sous forme textuelle</returns>
        /// <remarks>Si un argument facultatif est null, on le remplace par sa valeur par défaut</remarks>
        public static string EnTexte<T>(
            this IEnumerable<T> elems, string séparateur = " ",
            string texteAvant = "", string texteAprès = "")
        {
            séparateur = séparateur == null ? " " : séparateur;
            string str = null;
            for (int i = 0; i < elems.Count(); i++)
            {
                if (i != (elems.Count() - 1))
                    str += $"{texteAvant}{elems.ElementAt(i)}{texteAprès}{séparateur}";
                else
                    str += $"{texteAvant}{elems.ElementAt(i)}{texteAprès}";
            }
            return str;

        }
    }
}
