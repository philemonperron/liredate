﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using System.Collections.Generic;
using System.Linq;


namespace Prog2
{

    public partial class TesterDateBase
    {

 
        [TestMethod]
        public void _56d_NuméroDuMois4()
        {
            TesterNuméroDuMoisInsemsibleAuxAccents(NomDesMois.NumeroDuMois4);
            AreEqual(0, NomDesMois.NumeroDuMois4("déc"));
        }

    }
}
