﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using System.Collections.Generic;
using System.Linq;


namespace Prog2
{

    public partial class TesterDateBase
    {
   

        [TestMethod]
        public void _56a_NuméroDuMois1()
        {
            TesterNuméroDuMois(NomDesMois.NumeroDuMois1);
            AreEqual(0, NomDesMois.NumeroDuMois1("Janvier"));
        }

        [TestMethod]
        public void _56b_NuméroDuMois2()
        {
            TesterNuméroDuMois(NomDesMois.NumeroDuMois2);
            AreEqual(0, NomDesMois.NumeroDuMois2("Janvier"));
        }

        [TestMethod]
        public void _56c_NuméroDuMois3()
        {
            TesterNuméroDuMoisInsemsibleÀLaCasse(NomDesMois.NumeroDuMois3);
            AreEqual(0, NomDesMois.NumeroDuMois3("decembre"));
        }

    }
}
