﻿using System;
using Prog2;

namespace AfficherDateConstante
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Afficher Date Constante";
            var dateConst = new DateConstante (2001, 9,  11);
            ConsolePlus.Afficher("        ToString", dateConst);
            ConsolePlus.Afficher("     EntexteLong", dateConst.EnTexteLong());
            ConsolePlus.Afficher("    JourDeLAnnee", dateConst.JourDeLAnnée);
            ConsolePlus.Afficher(" JourDeLaSemaine", dateConst.JourDeLaSemaine);
//            dateConst.Incrementer(365);
//            ConsolePlus.Afficher("  1 an plus tard", dateConst);
//            dateConst.Decrementer(365);
//            ConsolePlus.Afficher("  1 an plus tot", dateConst);
//            dateConst.Mois--;
//            ConsolePlus.Afficher("  1 mois plus tot", dateConst);
            dateConst.Jour--;
            ConsolePlus.Afficher("  1 jour plus tot", dateConst);
            Console.WriteLine();
            
            ConsolePlus.Afficher("Cloner OK", dateConst.Cloner() is DateConstante);
            ConsolePlus.Afficher("Dupliquer OK", dateConst.Dupliquer() is DateConstante);
            
        }
    }
}