﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using Prog2;

namespace AfficherRandom
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Afficher Random --- Nombres Aleatoire";

            ConsolePlus.ColorWriteLine(ConsoleColor.Cyan, "Entrez la germe: ");
            var germe = Convert.ToInt32(Console.ReadLine());

            ConsolePlus.ColorWriteLine(ConsoleColor.Cyan, "Entrez le nombre de chiffres à afficher: ");
            var nombreChiffres = Convert.ToInt32(Console.ReadLine());

            ConsolePlus.ColorWriteLine(ConsoleColor.DarkYellow, "Germe: {0}", germe);
            var random = new Random(germe);

            var nombresAleatoires = new List<int>();

            ConsolePlus.ColorWriteLine(ConsoleColor.Cyan, "Nombres aleatoires de 1 à 6: ");
            for (int i = 0; i < nombreChiffres; i++)
            {
                var dé6 = random.Next(1, 7);
                Console.Write(" {0}", dé6);
                nombresAleatoires.Add(dé6);
            }
            Console.WriteLine();


            var counts = nombresAleatoires
                .OrderBy(x => x)
                .GroupBy(x => x)
                .ToDictionary(g => g.Key, g => g.Count())
                .ToList();

            foreach (var nombre in counts)
            {
                ConsolePlus.ColorWriteLine(ConsoleColor.DarkMagenta, $"#{nombre.Key} = {nombre.Value}");
            }
        }
    }
}