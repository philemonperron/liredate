﻿using System;
using System.Collections.Generic;
using Prog2;

namespace AfficherÉnumérable
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Afficher Enumerable";

            var date = new Date();

            object objet = date;
            IEquatable<Date> equatable = date;
            IComparable<Date> comparable = date;
            IFormattable formattable = date;

            ConsolePlus.Afficher("Meme objet",
                ReferenceEquals(date, objet) &&
                ReferenceEquals(objet, equatable) &&
                ReferenceEquals(equatable, comparable) &&
                ReferenceEquals(comparable, formattable));

            ICalendrier cal = new Calendrier(2000, Mois.Mai);


            //IEnumerable polymorph 1
            int[] impairs = new[] {1, 3, 5, 7, 9};
            List<int> pairs = new List<int> {2, 4, 6, 8, 10};
            IEnumerable<int> entiers;
            entiers = impairs;
            entiers = pairs;

            //Ienumerable et foreach
            Console.Write("\nEntiers:");
            foreach (var entier in entiers)
            {
                Console.Write(entier + " ");
            }

            entiers = impairs;
            foreach (var entier in entiers)
            {
                Console.Write(entier + " ");
            }

            Console.WriteLine();

            //Parametre IEnumerable 1
            void WriteAll(IEnumerable<int> p_entiers, string separateur = " ")
            {
                foreach (var entier in p_entiers)
                {
                    Console.Write(entier + separateur);
                }
            }

            //Parametre IEnumerable 2
            string EnTexte(IEnumerable<int> p_entiers, string separateur = " ")
            {
                return string.Join(separateur, p_entiers);
            }

            Console.WriteLine($"\nEntiers {EnTexte(pairs)} {EnTexte(impairs)}");

            Console.Write("Entiers ");
            WriteAll(pairs);
            WriteAll(impairs);
            Console.WriteLine();
            
            //IEnumerable polymorphe 2
            char[] voyelles = new[] {'a', 'e', 'i', 'o', 'u'};
            List<char> consonnes = new List<char> {'b','c','d','f','g'};
            string chiffres = "12345";
            IEnumerable<char> caracteres;
            caracteres = voyelles;
            caracteres = consonnes;
            caracteres = chiffres;
            
            //Parametre IEnumerable 3
            void WriteAllChar(IEnumerable<char> chars, string separateur = " ")
            {
                foreach (var c in chars)
                {
                    Console.Write(c + separateur);
                }
            }
            Console.Write("\nCaracteres ");
            WriteAllChar(voyelles);
            WriteAllChar(consonnes);
            WriteAllChar(chiffres);
            Console.WriteLine();

            //Methode generique
            void WriteAny<T>(IEnumerable<T> elems, string separateur = " ")
            {
                foreach (var elem in elems)
                {
                    Console.Write(elem + separateur);
                }
            }
            
            Console.Write("\nAny: ");
            WriteAny<int>(new [] {1, 2, 3});
            WriteAny(new List<bool>{true, false});
            WriteAny("C#");
            WriteAny(new[] { date.Hier, date.Demain});
            Console.WriteLine();
            
        }
    }
}