﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Prog2;

namespace AfficherObjet
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Afficher Objet";

            object obj = new object();
            ConsolePlus.ColorWriteLine(ConsoleColor.Cyan, "obj: {0}", obj);

            Date date = new Date(2011, 09, 11);
            ConsolePlus.ColorWriteLine(ConsoleColor.DarkCyan, "Date: {0}", date);

            obj = date;
            date = (Date) obj;
            ConsolePlus.ColorWriteLine(ConsoleColor.Cyan, "obj: {0}", obj);

            obj = "C#";
            Console.Out.WriteLine("obj = {0}", obj);

            obj = 5;
            try
            {
                date = (Date) obj;
            }
            catch (InvalidCastException)
            {
            }

            Console.Out.WriteLine("obj = {0}", obj);

            obj = null;
            Console.Out.WriteLine("obj = {0}", obj);

            //S'informer sur des objets
            ConsolePlus.Colorwrite(ConsoleColor.DarkCyan, InfoObjet(new Date(2011, 09, 11)));
            ConsolePlus.Colorwrite(ConsoleColor.DarkGreen, InfoObjet(new Calendrier(2019, Mois.Mars)));
            ConsolePlus.Colorwrite(ConsoleColor.DarkMagenta, InfoObjet("C#"));
            ConsolePlus.Colorwrite(ConsoleColor.Cyan, InfoObjet(new object()));
            ConsolePlus.Colorwrite(ConsoleColor.Blue, InfoObjet(new List<int>()));
            ConsolePlus.Colorwrite(ConsoleColor.Magenta, InfoObjet(new int[0]));
            ConsolePlus.Colorwrite(ConsoleColor.DarkRed, InfoObjet(new ArgumentException()));
            ConsolePlus.Colorwrite(ConsoleColor.DarkYellow, InfoObjet(5));
            ConsolePlus.Colorwrite(ConsoleColor.Yellow, InfoObjet(true));
            ConsolePlus.Colorwrite(ConsoleColor.Green, InfoObjet(Mois.Mars));
            ConsolePlus.Colorwrite(ConsoleColor.DarkGreen, InfoObjet(new Calendrier(2019, Mois.Mars)));


            //Operateur is
            obj = true;
            Debug.Assert(obj is object);
            Debug.Assert(obj is Object);
            Debug.Assert(obj is ValueType);
            Debug.Assert(obj is Boolean);
            Debug.Assert(obj is bool);
            Debug.Assert(!(obj is int));
            Debug.Assert(!(obj is Mois));
            Debug.Assert(!(obj is null));
            
//            Debug.Assert((obj.GetType() == typeof(int)));
//            Debug.Assert((obj.ToString() == "10"));
//            Debug.Assert((obj.Equals(10)));
//            Debug.Assert((obj != (object)10));
//            Debug.Assert(((int)obj == 10));
            
            //deboxage
//            int j = (int) obj;
//            j++;
            
            //Double boxage
//            Debug.Assert(object.Equals(object(10), (object)10));
            Debug.Assert(Equals(10, 10));
            Debug.Assert(!object.ReferenceEquals(10, 10));
            
            //Tableau d'objets
            var objets = new object[]
            {
                null, 100, true, Mois.Janvier, Date.Aléatoire(new Random(), 1900, 2000),
                new Calendrier(2019, Mois.Mars),
            };

            Console.Out.WriteLine("\nTableau d'objets:");
            foreach (var chose in objets)
            {
                Console.Out.WriteLine("* {0}", DateUtil.EnTexte(chose));
            }
            
            //ToString
            Console.WriteLine("\nTableau d'objets (ToString):");
            foreach (var objet in objets)
            {
                ConsolePlus.ColorWriteLine(ConsoleColor.DarkYellow, " * " + objet);
            }
            
            Console.WriteLine("\nTableau d'objets (ToString implicite 1)");
            foreach (var objet in objets)
            {
                ConsolePlus.ColorWriteLine(ConsoleColor.DarkRed, " * " + objet);
            }
  
            Console.WriteLine("\nTableau d'objets (ToString implicite 2)");
            foreach (var objet in objets)
            {
                ConsolePlus.ColorWriteLine(ConsoleColor.DarkMagenta, " * {0}", objet);
            }
            
            
            
            //Equals
            Debug.Assert(objets.Contains(true));
            Debug.Assert(!objets.Contains(false));
            Debug.Assert(objets.Contains(100));
            Debug.Assert(!objets.Contains(99));
            Debug.Assert(objets.Contains(new Calendrier(2019, Mois.Mars)));
            Debug.Assert(!objets.Contains(new Calendrier(2018, Mois.Mars)));
            Debug.Assert(Equals(new Calendrier(2019, Mois.Mars), new Calendrier(2019, Mois.Mars)));
            Debug.Assert(!Equals(new Calendrier(2019, Mois.Mars), new Calendrier(2018, Mois.Mars)));
            Debug.Assert(new Calendrier(2019, Mois.Mars) == new Calendrier(2019, Mois.Mars));
            Debug.Assert(new Calendrier(2019, Mois.Mars) != new Calendrier(2018, Mois.Mars));
            
            
        }
        
        

       
        public static string InfoObjet(object obj)
        {
            var parent = obj.GetType().BaseType;
            var grandParent = parent?.BaseType;
            var arriereGrandParent = grandParent?.BaseType;


            if (obj == null)
                return "\nobjet: null\n";
            return
                $"\n 1. objet:              {DateUtil.EnTexte(obj)}" +
                $"\n 2. type d'objet        {obj.GetType()}" +
                $"\n 3. type de Date        {typeof(Date)}" +
                $"\n 4. type == Date        {DateUtil.EnTexte(typeof(Date) == obj.GetType())}" +
                $"\n 5. nom du type         {obj.GetType().Name}" +
                $"\n 6. parent              {DateUtil.EnTexte(obj.GetType().BaseType)}" +
                $"\n 7. parent == objet     {DateUtil.EnTexte(obj.GetType().BaseType == typeof(object))}" +
                $"\n 8. grand-parent:       {DateUtil.EnTexte(grandParent)}" +
                $"\n 9. arr-grand-parent:   {DateUtil.EnTexte(arriereGrandParent)}" +
                "\n";
        }
    }
}